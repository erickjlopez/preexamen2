const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");

router.get("/", (req, res) => {
	//res.send("<h1>Iniciamos con express</h1>");
	res.render("index.html", {});
});

router.get("/agencia", (req, res) => {
	//res.send("<h1>Iniciamos con express</h1>");
	const valores = {
		numBoleto: req.query.numBoleto,
		destino: req.query.destino,
		nombre: req.query.nombre,
		añosCump: req.query.añosCump,
		tipoViaje: req.query.tipoViaje,
		precio: req.query.precio,
		subtotal: req.query.subtotal,
		impuesto16: req.query.impuesto16,
		descuento: req.query.descuento,
		total: req.query.total,
		descuentoTotal: req.query.descuentoT
	};
	res.render("agencia.html", valores);
});

router.post("/agencia", (req, res) => {
	//res.send("<h1>Iniciamos con express</h1>");
	const valores = {
		numBoleto: req.body.numBoleto,
		destino: req.body.destino,
		nombre: req.body.nombre,
		añosCump: req.body.añosCump,
		tipoViaje: req.body.tipoViaje,
		precio: req.body.precio,
		subtotal: req.body.subtotal,
		impuesto16: req.body.impuesto16,
		descuento: req.body.descuento,
		total: req.body.total,
		descuentoTotal: req.body.descuentoT
	};
	res.render("agencia.html", valores);
});

module.exports = router;
